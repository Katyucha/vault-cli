import json
import base64
import requests
import configparser
from enum import Enum
from time import sleep
import argparse
import os, subprocess, tempfile
import datetime
import cmd

def isBase64(string):
  try:
    if string == "None":
      return False
    else:
      base64.b64decode(string)
      return True
  except:
    return False

def listHistory(operation):
  *head, tail = operation.conf.split('/')
  headers = {"X-Vault-Token": VAULT_TOKEN}
  urlGet = vaultUrl + '/v1/secret/' + '/'.join(head) + "?list=true"
  f = requests.get(urlGet,  headers=headers)
  slf = f.json()["data"]["keys"]
  # filtrer sur les history
  result=[]
  for a in slf:
    if "history-" in a:
      result.append(a)
  print(' '.join(result))


def writeFile(myMessage):
  f, fname = tempfile.mkstemp()
  os.write(f, str.encode(myMessage.message.decode("utf-8")))
  os.close(f)
  cmd = os.environ.get('EDITOR', 'vi') + ' ' + fname
  subprocess.call(cmd, shell=True)
  returnFile = ""
  with open(fname, 'r') as f:
     returnFile=f.read()
  os.unlink(fname)
  return returnFile

def saveFile(message, operation, history):
  if history:
    historyFile(message, operation)

  headers = {"X-Vault-Token": VAULT_TOKEN, "Content-type": "application/json"}
  base64Valeur = base64.b64encode(str.encode(message.newValue))
  payload = { operation.key : base64Valeur.decode("utf-8") }
  urlPost = vaultUrl + '/v1/secret/' + operation.conf
  f = requests.put(urlPost , data=json.dumps(payload), headers=headers)
  # TODO retour du put

def historyFile(message, operation):
  headers = {"X-Vault-Token": VAULT_TOKEN, "Content-type": "application/json"}
  dateHistory = "history-"+ datetime.datetime.now().strftime('%Y%m%d%H%M%S')

  *head, tail = operation.conf.split('/')
  head.append(dateHistory)
  head.append(tail)
  newPath = '/'.join(head)
  historyB64Valeur = base64.b64encode(str.encode(message.message.decode("utf-8")))
  historyPayload = { operation.key : historyB64Valeur.decode("utf-8") }
  urlHistoryPost = vaultUrl + '/v1/secret/'+ newPath + '/' +operation.conf
  hF = requests.put(urlHistoryPost , data=json.dumps(historyPayload), headers=headers)
  # TODO  retour du put


def listFromVault(operation):
  headers = {"X-Vault-Token": VAULT_TOKEN}
  urlGet = vaultUrl + '/v1/secret/' + operation.conf + "?list=true"
  f = requests.get(urlGet, headers=headers)
  returnJson = f.json()
  if "data" in returnJson:
    slf = returnJson["data"]
    return slf
  else:
    return ("ERROR: no " + operation.conf + " found")

def getValueFromVault(operation):
  headers = {"X-Vault-Token": VAULT_TOKEN}
  urlGet = vaultUrl + '/v1/secret/' + operation.conf
  f = requests.get(urlGet, headers=headers)
  print(f.json())
  slf = f.json()["data"]
  if operation.key in slf:
    return slf[operation.key]
  else:
    return 'None'

class Message:
  def __init__(self, valeur):
    self.newValue = ""
    if isBase64(valeur):
      self.type = "base64"
      self.message = base64.b64decode(valeur)
    else:
      self.type = "simple"
      self.message = valeur

  def setNewValue(self, value):
    self.newValue = value
    return self

class Operation:
  def __init__(self, line):
    readLine = line.split(" ")
    self.conf = readLine[0]
    try:
      self.key = readLine[1]
    except:
      self.key = "None"

class CoreCli(cmd.Cmd):

  prompt = "vault-cli $>"
  intro = "Here's a new CLI for Vault"

  def do_write(self, line):
    """write path key
      Modifie la valeur de la cle de l'arborescence"""
    myOperation = Operation(line)
    myMessage = Message(getValueFromVault(myOperation))
    saveFile(
      myMessage.setNewValue( writeFile(myMessage) ),
      myOperation,
      historyParams
    )

  def do_ls(self, line):
    """ls path
      Lis l'arborescence"""
    myOperation = Operation(line)
    myMessage = Message(listFromVault(myOperation))
    print(myMessage.message)

  def do_read(self, line):
    myOperation = Operation(line)
    if len(line.split(" ")) < 3:
      self.help_read()
    else:
      myMessage = Message(getValueFromVault(myOperation))
      if myMessage.type == "base64":
        print(myMessage.message.decode("utf-8"))
      else:
        print(myMessage.message)

  def do_history(self, line):
    """history path
      Liste les dates des sauvegardes"""
    listHistory(Operation(line))

  def do_EOF(self, line):
    return True

  def postloop(self):
    print()

  def help_read(self):
    print('\n'.join([
      'read path key',
      'read the key in path',
    ]))

  def do_quit(self, line):
    exit(0)

  def do_exit(self, line):
    exit(0)

if __name__ == '__main__':

  config = configparser.ConfigParser()
  config.read('env')
  if config['DEFAULT']:
    VAULT_TOKEN = config['DEFAULT']['VAULT_TOKEN']
    vaultUrl = config['DEFAULT']['vaultUrl']
    historyParams = config['DEFAULT']['history']
  else:
    print("EXIT: erreur de configuration")
    exit(255)

  CoreCli().cmdloop()
