## vault cli

## Fichier de config : env

```
[DEFAULT]
VAULT_TOKEN="xxxxxxx"
vaultUrl = "http://localhost:8200"
history=true
```


### Features

* pouvoir se promener dans l'arborescence
* modification des clés / fichiers base64 via vi
* historisation des clés et fichiers
* recherche de dossiers
* interface graphique (non, je rigole :P)
