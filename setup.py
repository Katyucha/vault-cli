#!/usr/bin/env python

from distutils.core import setup

setup(name='vault-cli',
      version='0.9',
      description='CLI pour vault',
      author='Sylvain LUCE',
      author_email='katyucha@alpha.caliopen.org',
      url='',
      packages=['distutils', 'distutils.command', 'requests'],
     )
